package features;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 *
 */
public class GridStepDefs {

    private WebDriver driver = null;

    private GridHub gridHub;
    private URL hubURL;
    private URLConnection connection;

    @Given("^I perform a selenium test$")
    public void I_perform_a_selenium_test() throws Throwable {
        gridHub = new GridHub();
        assertTrue(gridHub != null);
    }

    @When("^I contact the hub$")
    public void I_contact_the_hub() throws Throwable {

        hubURL = gridHub.getURL();

        if (hubURL != null) {

            try {
                connection = hubURL.openConnection();
            } catch (IOException e) {
                fail();
            }

        } else {
            fail();
        }

        if (connection != null) {
            try {
                connection.connect();
            } catch (IOException e) {
                fail();
            }
        } else {
            fail();
        }
    }

    @Then("^I should receive a reply from hub$")
    public void I_should_receive_a_reply_from_hub() throws Throwable {
        String contentType = null;

        try {
            contentType = connection.getContentType();
        } catch (Exception e) {
            fail();
        }
        assertEquals("application/json;charset=UTF-8", contentType);

    }

    @When("^I request a \"([^\"]*)\" driver$")
    public void I_request_a_driver(String desiredBrowserLabel) throws Throwable {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(desiredBrowserLabel);

        try {
            driver = new RemoteWebDriver(gridHub.getURL(), capabilities);
        } catch (MalformedURLException e) {
            fail("malformed url exception trying to get new driver from remote");
        }
    }

    @Then("^I should be able to use \"([^\"]*)\" driver$")
    public void I_should_be_able_to_use_driver(String expectedBrowserName) throws Throwable {
        // assert we are using the desired browser
        Capabilities capabilities = ((RemoteWebDriver) driver).getCapabilities();
        assertEquals(expectedBrowserName, capabilities.getBrowserName());

        // assert the driver is able to perform basic tasks
        driver.navigate().to("http://www.google.com");
        assertEquals("Google", driver.getTitle());
    }

}
